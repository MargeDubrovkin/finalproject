from django import template
from django.shortcuts import render
from django.template.defaultfilters import stringfilter
from django.template import Template

register = template.Library()

@register.filter
def in_attendee_list(attendee_list, attendee):
    return attendee_list.filter(attendee=attendee)