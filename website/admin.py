from django.contrib import admin
from .models import Category, Event, Comment, EventAttendee

# Register your models here.
@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Event)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['category', 'title', 'date_started', 'date_ended',
    'event_organizer', 'description']
    list_filter = ['title', 'event_organizer']
    
    
@admin.register(Comment)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['event', 'body', 'created_on']
    list_filter = ['event', 'body']
    

@admin.register(EventAttendee)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['event', 'attendee', 'registered_on']
    list_filter = ['event', 'attendee']