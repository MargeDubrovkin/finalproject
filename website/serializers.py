from rest_framework import serializers

from .models import Category, Event

class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = ('id','name', 'slug', 'date_added')


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ('id','title', 'date_started', 'date_ended', 'event_organizer', 'description')