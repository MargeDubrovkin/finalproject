from urllib import request
from django.forms import ModelForm
from django.shortcuts import get_object_or_404, render, redirect
from .models import Category, Event, Comment, EventAttendee
from django.views.generic.detail import DetailView
from django.views.generic import ListView, DetailView
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.views import View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.urls import reverse_lazy
from django.contrib.auth.views import LoginView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .forms import CommentForm, UserCommentForm, EventAttendeeForm, EventCreationForm, EventUpdateForm
from django.contrib.auth import login

from rest_framework import viewsets
from .serializers import CategorySerializer, EventSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response

from django.utils import timezone
from datetime import datetime
from datetime import date


class CustomLoginView(LoginView):
    template_name='website/login.html'
    fields='__all__'
    redirect_authenticated_user=True

    def get_success_url(self):
        return reverse_lazy('events_all')


class RegisterPage(FormView):
    template_name='website/register.html'
    form_class=UserCreationForm
    redirect_authenticated_user=True
    success_url=reverse_lazy('events_all')

    def form_valid(self, form):
        user=form.save()
        if user is not None:
            login(self.request, user)
        return super(RegisterPage, self).form_valid(form)


def events_all(request):
    events=Event.objects.all()
    return render(request, 'website/events_all.html', {'events':events})


def my_organized_events(request):
    if request.user.is_authenticated:
        organized_events=Event.objects.filter(event_organizer=request.user.id)
        return render(request, 'website/my_organized_events.html', {'organized_events': organized_events})
    else:
        return render(request, 'website/events_all.html', {})


def my_attendances(request):
    if request.user.is_authenticated: 
        attendances=EventAttendee.objects.filter(attendee=request.user.id)
        events=Event.objects.filter(event_organizer=request.user.id)
        return render(request, 'website/my_attendances.html', {'attendances': attendances, 'events': events})
    else:
        return render(request, 'website/events_all.html', {})
        

def categories(request):
    return {'categories': Category.objects.all()}


def category_list(request, category_slug):
    category=get_object_or_404(Category, slug=category_slug)
    events=Event.objects.filter(category=category)
    return render(request, 'website/category.html', {'category': category, 'events': events})


class EventsAllList(ListView):
    model =Event
    template_name ='website/events_all.html'
    context_object_name='events'

    def get_queryset(self):
        return Event.objects.filter(date_started__gte=timezone.now())


    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        #context['events']=context['events'].filter(user=self.request.user)
        
        search_input=self.request.GET.get('search-area') or ''
        if search_input:
            context['events']=context['events'].filter(title__startswith=search_input)
        context['search_input']=search_input
        return context


class EventDetailList(DetailView):
    model=Event
    context_object_name = 'event'
    template_name = 'website/event_detail.html'
 

class EventCreate(CreateView):
    model=Event
    #fields=['category', 'title', 'date_started', 'date_ended', 'description']
    form_class=EventCreationForm
    success_url=reverse_lazy('events_all')

    def form_valid(self, form):
        form.instance.event_organizer_id = self.request.user.id
        return super().form_valid(form)


class EventUpdate(UpdateView):
    model = Event
    #fields=['category', 'title', 'date_started', 'date_ended', 'description']
    form_class = EventUpdateForm
    success_url = reverse_lazy('events_all')

    def form_valid(self, form):
        form.instance.event_organizer_id = self.request.user.id
        return super().form_valid(form)


class DeleteView(DeleteView):
    model = Event
    context_object_name='event'
    template_name = 'website/event_confirm_delete.html'
    success_url=reverse_lazy('events_all')


class AddCommentView(CreateView):
    model=Comment
    form_class=CommentForm
    template_name='website/add_comment.html'
    
    def form_valid(self, form):
        form.instance.author_id = self.request.user.id
        form.instance.event_id=self.kwargs['pk']
        return super().form_valid(form)
    
    success_url=reverse_lazy('events_all')


class AttendeeView(CreateView):
    model=EventAttendee
    form_class=EventAttendeeForm
    #fields='__all__'
    #template_name='website/eventattendee_form.html'
    success_url=reverse_lazy('events_all')

    
    def form_valid(self, form):
        form.instance.event_id=self.kwargs['pk']
        return super().form_valid(form)
  
    
class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all().order_by('name')
    serializer_class = CategorySerializer


def attendance_delete_view(request, id):
    obj = get_object_or_404(EventAttendee, id=id)
    if request.method == "POST":
        obj.delete()
        return redirect('website/events_all.html')
    context = {
        "object": obj
    }
    return render(request, 'website/eventattendee_confirm_delete.html', context)
    

"""
class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all().order_by('title')
    serializer_class = EventSerializer
"""

@api_view(['GET'])
def apiEventList(request):
    events=Event.objects.all()
    serializer=EventSerializer(events, many=True)
    return Response(serializer.data)


@api_view(['POST'])
def apiEventUpdate(request, pk):
    event=Event.objects.get(id=pk)
    serializer=EventSerializer(instance=event, data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)


def unsubscribe_event(request, pk, event):
    message ="You have unsubscribed from this event!"
    EventAttendee.objects.get(id=pk).delete()
    return redirect('event_detail', pk = event)



