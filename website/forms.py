from .models import Comment, EventAttendee, Event
from django.forms import ModelForm
from django import forms
from django.contrib.admin.widgets import AdminDateWidget


class DateInput(forms.DateInput):
    input_type = 'date'


class EventCreationForm(ModelForm):
    class Meta:
        model = Event
        fields = ['category', 'title', 'date_started', 'date_ended', 'description']
        widgets = {
            'date_started': DateInput(),
            'date_ended': DateInput(),
        }

class EventUpdateForm(ModelForm):
    class Meta:
        model = Event
        fields = ['category', 'title', 'date_started', 'date_ended', 'description']
        widgets = {
            'date_started': DateInput(),
            'date_ended': DateInput(),
        }


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('body', )



class UserCommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ('body', )


class EventAttendeeForm(forms.ModelForm):
    class Meta:
        model = EventAttendee
        fields = ['event', 'attending']