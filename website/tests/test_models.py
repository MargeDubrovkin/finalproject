from django.test import TestCase

from website.models import Category, Event

class TestCategoriesModel(TestCase):

    def setUp(self):
        self.data1=Category.objects.create(name='virtual', slug='virtual')

    def test_self_category_model_return(self):
        data=self.data1
        self.assertEqual(str(data), 'virtual')
