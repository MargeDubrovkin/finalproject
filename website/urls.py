from django.urls import path, include
from . import views
from .views import EventDetailList, EventsAllList, CustomLoginView, RegisterPage, attendance_delete_view
from .views import EventCreate, EventUpdate, DeleteView, AddCommentView, AttendeeView,unsubscribe_event
#from .views import EventViewSet
from django.contrib.auth.views import LogoutView

from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'categories', views.CategoryViewSet)


urlpatterns = [
    path('login/',CustomLoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(next_page='login'), name='logout'),
    path('register/', RegisterPage.as_view(), name='register'),
    
    
    path('', EventsAllList.as_view(), name='events_all'),
    path('details/<int:pk>/', EventDetailList.as_view(), name='event_detail'),
    path('search/<slug:category_slug>/', views.category_list, name='category_list'),
    path('event-create/',EventCreate.as_view(), name='event-create'),
    path('event-update/<int:pk>/', EventUpdate.as_view(), name='event-update'),
    path('event-delete/<int:pk>/', DeleteView.as_view(), name='event-delete'),
    path('details/<int:pk>/comment/', AddCommentView.as_view(), name='add-comment'),
    path('event-attend/<int:pk>/attend/cl', AttendeeView.as_view(), name='event-attend'),
    path('attend-delete/<int:pk>/delete/', views.attendance_delete_view, name='attend-delete'),
    path('unsubscribe/<int:pk>/<int:event>/', unsubscribe_event, name="unsubscribe" ),
    path('search/<slug:category_slug>/',views.category_list, name='category_list'), 

    path('', include(router.urls)),
    path('api-events/',views.apiEventList, name='apieventlist'),
    path('api-event/update/<int:pk>/',views.apiEventUpdate, name='apieventupdate'),
    path('my-attendances/', views.my_attendances, name='my_attendances'),
    path('my-organized-events/', views.my_organized_events, name='my_organized_events'),
    
]

urlpatterns+=router.urls