from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from datetime import date
from datetime import datetime



class Category(models.Model):
    name=models.CharField(max_length=200, null=True)
    slug=models.SlugField(max_length=200, unique=True)
    date_added=models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural='categories'

    def get_absolute_url(self):
        return reverse('category_list', args=[self.slug])

    def __str__(self):
        return self.name


class Event(models.Model):
    category=models.ForeignKey(Category, related_name='event', on_delete=models.CASCADE)
    title=models.CharField(max_length=200)
    date_started=models.DateTimeField(auto_now_add=False, auto_now=False, blank=True, null=True)
    date_ended=models.DateTimeField(auto_now_add=False, auto_now=False, blank=True, null=True)
    event_organizer=models.ForeignKey(User,related_name='event_organizer', default=User, on_delete=models.CASCADE)
    description=models.TextField(blank=True)
    

    class Meta:
        verbose_name_plural='events'
        ordering = ['date_started']

    

    def get_absolute_url(self):
        return reverse('event_detail', args=[str(self.slug)])

    def __str__(self):
        return self.title

    @property
    def days_until_event(self):
        today=date.today()
        days_until=self.date_started.date()-today
        days_until_without_time=str(days_until).split(",", 1)[0]
        return days_until_without_time

    @property
    def days_past(self):
        today=datetime.today()
        if self.date_started.timestamp() < today.timestamp():
            event_time="Past"
        else:
            event_time="Future"
        return event_time

    @property
    def is_past_due(self):
        today=datetime.today()
        if self.date_started.timestamp() > today.timestamp():
            return True
        return False
        
        
class Comment(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    body = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='comments')

    class Meta:
        ordering = ['-created_on']

    def __str__(self):
        return '%s - %s' % (self.event.title, self.author)
    
class EventAttendee(models.Model):
    attendee = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    registered_on= models.DateTimeField(auto_now_add=True)
    event=models.ForeignKey(Event, on_delete=models.CASCADE, related_name='attendees')
    attending=models.BooleanField(default=False)

    def check_attendance(self):
        pass

    def __str__(self):
        return self.attendee
